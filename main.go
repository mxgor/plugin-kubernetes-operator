package main

import (
	"context"

	"github.com/spf13/viper"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-kubernetes-operator/kong"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-kubernetes-operator/kubernetes"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-kubernetes-operator/logger"
)

func main() {
	ctx, f := context.WithCancel(context.Background())

	viper.SetConfigFile(".env")
	viper.ReadInConfig()
	viper.AutomaticEnv()

	logger := logger.GetLogger()
	defer logger.Sync()

	err := kubernetes.InitializeKubernetes()

	if err != nil {
		logger.Error(err.Error())
		return
	}

	err = kubernetes.StartPluginObserver(ctx, kong.SyncKongService, kong.SyncKongServices)

	if err != nil {
		logger.Error(err.Error())
		return
	}

	defer f()

}
